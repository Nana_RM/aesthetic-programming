## Individual flowchart

For my individual flowchart, I have chosen to make a flowchart of my minix7. I have chosen to focus on the draw() and keyPressed() functions, since they contains if-statements and for loops. I believe getting an overview of these syntaxes can make an easier overview of how the program works. seen below is three pictures of my flowchart. 

**Overview of the flowchart:**

![flowchart1.1](flowchart1.1.png)

**First part of the flow chart:**
![flowchart1.2](flowchart1.2.png)

**Second part of the flowchart:**
![flowchart1.3](flowchart1.3.png)

## Flowcharts by studygroup 6

**What are the difficulties involved in trying to keep things simple at the communications level whilst maintaining complexity at the algorithmic procedural level?**
When flowcharting, it can be easy to get caught up in the little details and start pondering how exactly things should be executed on the code level in our program. For the sake of creating a more fruitful idea-generating environment, we therefore decided to create more overall flowcharts, showcasing the general ideas of our concepts, instead of lingering too much on syntactical detail.

**What are the technical challenges facing the two ideas and how are you going to address these?**
When brainstorming and creating the flowchartes, we haven’t focused on the technical aspects of the program, but rather the conceptual thinking. We wanted to create two superficial ideas and let the design become more specific, later in the design process. 

**In which ways are the individual and the group flowcharts you produced useful?**
Whilst brainstorming, a lot of abstract ideas and concepts are thrown around in the group. Creating a flowchart is a nice way to visualise and concretise the ideas and get a sense that all group members are on the same page in terms of the more concrete execution of the idea.

**My own thoughts about using a flowchart:**
During the creation of our flowcharces, aswell as my own, I notices that flowcharces can help getting a better overview of an already made program, aswell as before creating a program. Since we only had some conceptual thoughts and not a concrete plan for how we would like to make the program, it was dfficult to create a flowchart that was going into depth with the concept. I believe it will get easier when we have a better idea of what we would like the program to be like and then can revisit the flowchart and change it. 

### General description of our ideas: 

**flowchart 1: Artwork generator**

The artwork generator is a program in which the computer is given an api containing photos organized into categories. The computer has also been given templates of larger photos/symbols. The computer will then create a photo mosaic of the larger photos/symbols made of many of the smaller photos from the api. In this way we are able to create a mosaic containing two opposite ideas/political statements/etc. For example, one artwork could be a photo of the pride flag, made by many small photos of Jehovah's Witnesses. 

![flowchart2](flowchart2.png)

**Flowchart 2: Data query - The cultural depth of words and their synonyms. **

A user sends a request for a word to check its connected synonyms. The sotware clicks on the first available synonym and proceeds to the page the chosen synonym. The first synonym of the newly opened word description is then clicked, and the mentioned process iteration a prefixed number of times. 

![flowchart3](flowchart3.png)

The software deals with associations, and how different words can essentially frame the same object or phenomenon by carrying different cultural charges.  
The used words are retrieved from API’s containing words and the associations. 
For instance: 
https://wordassociations.net/en/api    
https://www.wordsapi.com/

## References

**References for the drawing of the flowcharces**
Soon Winnie & Cox, Geoff, "Algorithmic procedures", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 215
