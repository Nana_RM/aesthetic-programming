
function setup(){
  createCanvas(1000,700);
  background(201,225,255);
}


function draw(){
//hair
 fill(65,46,17);
 stroke(50,35,13);
  circle(500,350,300);

//head
fill(88,71,46);
rect(425, 370, 160, 140, 0, 0, 100, 100);

//mouth
fill(500);
stroke(50);
strokeWeight(1);
 arc(505, 450, 80, 70, 0, PI);

//left eye
fill(0);
noStroke();
 circle(460,400,20);

//right eye
 fill(0);
  circle(550,400,20);

  //eyebrow left
  fill(65,46,17);
  stroke(50,35,13);
  strokeWeight(2);
  rect(435, 360, 50, 20, 20);

  //eyebrow right
  fill(65,46,17);
  stroke(50,35,13);
  strokeWeight(2);
  rect(525, 360, 50, 20, 20);

//grey eye reflection (left)
  fill(179);
  noStroke();
   circle(462,397,5);

   //grey eyereflection (right)
   fill(179);
   noStroke();
    circle(552,397,5);
}
