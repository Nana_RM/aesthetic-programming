**Screenshot:**

![screenshot](Skærmbillede_2021-02-11_kl._20.32.13.png)

**Program link:** 
https://nana_rm.gitlab.io/aesthetic-programming/minix2/minix2.1/

**Code link:**
https://gitlab.com/Nana_RM/aesthetic-programming/-/blob/master/minix2/minix2.1/sketch.js

## What have I learned?
For my minix2.1 I wanted to focus on geometric shapes and the representation of etchnic minorities through emojis. I have programmed an emoji with african character traids. The emoji is made with circles, rectangles and arcs. Its is filled with different colours of brown and colors of the grey scale.

_What I have learned_
In my previous miniX, I worked with circles, colours and strokes. In this miniX, I have worked with circles but rectangels and arcs as well. This have given me an insight of how to shape the conors of a rectangle and how to program an arc. By using differen shapes in order to create a emoji, I have also learned the importance of the location of the code - if You want to place an object in a higher layer, it has to be on a higher number of the codelines in Atom. 

## Emojis in a social and cultural context
A lot of emojis on Androids and Iphones is programmed to be white people. In the recent years, companies have created a variaty of emojies by adding different skin and hair colours. Even though the newest emojis might have different skin colours, they don't have characteristics that represents their ethnicity, such as hair, clothing, facial shapes ect. Therefore I wanted to create an emoji that represents an ethnic minority, by adding the afro textured hair to and emoji. 
