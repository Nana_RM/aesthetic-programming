## The Emojies of Minorities I 
_The Emojies of Minorities I_ is a part of an emoji collection that tries to bring the discussion of the underrepresentation of minorities in emojies to life. The emoji, seen below, represent the Afro American people. It is made with african american traids by creating shapes such as circles, rectangles and arcs, that is filled with different colours of brown and colors of the grey scale. This gives the emoji afro hair.

![Screenshot](minix2_minix2.1_Skærmbillede_2021-02-11_kl._20.32.13.png)

**Links:**

Link to program: https://nana_rm.gitlab.io/aesthetic-programming/minix2/minix2.1/ 

Link to code: https://gitlab.com/Nana_RM/aesthetic-programming/-/blob/master/minix2/minix2.1/sketch.js 

### Thoughts during the process

**Conceptual thoughts**
In a class discussion that concerned emojies and underrepresentation of minorities, one of my class mates mentioned the lack of variation of the emojies hair on iphones and androids. As she elaborated, the emojies always have flat hair - not curly, afro etc. I choose to use this issue for my minix2. 

Emojies haven't always had been focused on representing people. As Femke Snelting mentions in her presentation concerning unicode, the need for emojies that represents the individual has been increased through the years. In the recent years, companies have created a variaty of emojies by adding different skin and hair colours. Even though the newest emojis might have different skin colours, they don't have characteristics that represents their ethnicity, such as hair, clothing, facial shapes ect. Therefore I wanted to create an emoji that represents an ethnic minority, by adding the afro textured hair to and emoji. By adding a diffrent skin color as well as hairstyle to an emoji, I have tried to represent the afro americans. 

Even though I believe it is important to represent minorities in the debate of emojies, I believe that this need for overrepresentation also might have its downsides. The amount of emojies becomes huge and it can get confusing when looking for an emoji on the phone. This raises the question: What type of traids should we select when creating emojies and who should we deselect?

**Thought during the programming**
In my previous miniX, I worked with circles, colours and strokes. For this miniX, I have worked with circles, rectangels and arcs. This have given me an insight of how to shape the conors of a rectangle and how to program an arc. By using differen shapes in order to create a emoji, I have also learned the importance of the location of the code - if You want to place an object in a higher layer, it have to be on a higher number of the codelines in Atom.


## The Emojies of Minorities II 
_The Emojies of Minorities II_ is a part of an emoji collection that tries to bring the discussion of the underrepresentation of minorities in emojies to life. The emoji, seen below, represents people with vision problems. To include people with vision problems, I have created a robot emoji with a robot sound that plays constantly. The emoji is created with circles, rectangles and lines of different colors of grey and red. 

 ![Screenshot](minix2_minix2.2_Skærmbillede_2021-02-15_kl._13.05.14.png)

 **Links**
Link to program: https://nana_rm.gitlab.io/aesthetic-programming/minix2/minix2.2/ 

Link to code: https://gitlab.com/Nana_RM/aesthetic-programming/-/blob/master/minix2/minix2.2/sketch.js 

### Thoughts during the process

**Conceptual thought**
For this emoji, I wanted to focus on people with visual problems, since they weren't a part of the class debate. I'm not sure if these kind of emojies have already been created, but thought it still would be important to put this in focus. Adding a sound to an emoji might as well change the context of a message, since it makes the meaning of the emoji more clear. Thereby it makes it harder for people to change the meaning of the emoji in the social and cultural context.


**Thought during the programming**
Since I already created another emoji, I had already learned to creates shapes in different colors as well as the importance of the placement of the code in Atom. When creating the emoji, I learned a lot about adding sound to a program. When adding sound to a program it is important to use the syntax _loadSound(name of the file)_ in order to load the file. The song can then be played in the setup function, where I also set the volume of the sound. For some reason the sound didn't always work - I cannot answer why i stil doesn't work instantly...

## Information

### Sources
- Snelting, F. (2016, may 16). _Modifying the Univseral_ [Video]. Youtube. https://www.youtube.com/watch?v=ZP2bQ_4Q7DY

### Task description
Explore shapes, geometries, and other related syntax (via p5.js references) and design two emojis, https://p5js.org/reference/.

### Questions to think about

- Describe your program and what you have used and learnt.
- How would you put your emoji into a wider social and cultural context that concerns a politics of representation, identity, race, colonialism, and so on? (Try to think through the assigned reading and your coding process, and then expand that to your own experience and thoughts - this is a difficult task, you may need to spend some time thinking about it).
