//OBS: The sound can be loud when the code is run and You might not be able to run
//on every browser - try Firefox.

//I declare 'song' as a variable.
var song;

function preload(){
  //In order to load a mp3 file, I assign a value to the 'song variable'.
  song = loadSound("file.mp3");
}

function setup(){
  //I create a canvas and add a colour to the backgroudn by using RGB.
  createCanvas(1400,700);
  background(255,254,183);
  //I use two functions on the variable 'song' in order to play the file and
  //set the volume.
  song.play();
  song.setVolume(0,5);
}

function draw(){
//In this part, I will create the robot//

//The head
stroke(200);
strokeWeight(5);
fill(225);
rect(500,250,200,150,20,20,20,20);

//The left antenna of the robot. OBS: I'm using lines instead of rectangles
//- it's just for practice.
stroke(200);
strokeWeight(10);
line(545,245,545,200);

stroke(200);
strokeWeight(5);
fill(225);
circle(545,200,30);

//The right antenna of the robot. OBS: I'm using lines instead of rectangles - it's just for practice
stroke(200);
strokeWeight(10);
line(655,245,655,200);

stroke(200);
strokeWeight(5);
fill(225);
circle(655,200,30);

//The mouth
//I'm using lerpColor, which calculates a color between the to two values (let from/to).
//The color is calculated by indicating a number between 0-1 with the syntax 'lerpColor'//
stroke(50);
strokeWeight(1);
colorMode();
let from = color(100);
let to = color(150);
colorMode();
let interA = lerpColor(from, to, 0.70);
let interB = lerpColor(from, to, 0.44);
fill(from);
rect(560, 340, 20, 30);
fill(interA);
rect(580, 340, 20, 30);
fill(interB);
rect(600, 340, 20, 30);
fill(to);
rect(620, 340, 20, 30);

//left eye
stroke(191,0,38);
strokeWeight(3);
fill(222,0,44);
rect(530,280,30,40,5,5,5,5);

//left shine in eye
noStroke();
fill(255,95,127);
rect(545,285,10,10,1,1,1,1);

//right eye
stroke(191,0,38);
strokeWeight(3);
fill(222,0,44);
rect(640,280,30,40,5,5,5,5);

//right eye
noStroke();
fill(255,95,127);
rect(655,285,10,10,1,1,1,1);

}
