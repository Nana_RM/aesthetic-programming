*Screenshot:**

![Screenshot](Skærmbillede_2021-02-15_kl._13.05.14.png)

**Program link:**

**Code link:**
https://gitlab.com/Nana_RM/aesthetic-programming/-/blob/master/minix2/minix2.2/sketch.js


## What have I learned?
For this miniX, I have made a robot emoji for people with hearing problems by adding sound to the emoji. The robot emoji consist of rectangles while it's antenna is made of lines and circles. When the program is opened, a robot sound playes nonstop 

I have learned the importance of the placement of the code in order to put the outcome in the right layer. When adding sound, the placement of the code also plays an importent part for the outcome. The sound has to be preloaded with the function preload by making it a variable. The mp3 file is played in the function setup.   

## Emojis in a social and cultural context
We often connect emojis with the sight of something on a screen but not a sound. To include people with vision problems, I have created an robot emoji with a robot sound that plays constantly.  
