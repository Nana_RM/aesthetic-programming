## Life-Sucker

_Life-Sucker_ is a work that brings awareness to our increasing use of smartphones. Smartphones has become an important part of our everyday lifes, due to the many functions that have been incorporated in them throughout the years. It connects us to friends and family, act as our personal guide through the city, acts as a camera etc. Even thought these functions might be seen as a good thing, it might have a negative impact on our lives. 


Link to program: https://nana_rm.gitlab.io/aesthetic-programming/minix3/

Link to the code: https://gitlab.com/Nana_RM/aesthetic-programming/-/blob/master/minix3/throbber.js

**Screenshots of the program:**
![Screen1](Screen1.png)
![Screen2](Screen2.png)

### How do the program work?
For this miniX I have choosen to create awareness of the average usage of our phones through the years and how it affects people. When a button on the screen is pressed, the circle of the thrubber resizes according to the number of minuts we uses our phones on average each day (the minimum consumption is around 30 min and the maximum around 255 min). The button releases a Messenger notification sound and adds a speech bobble with a text. Each year has a different text which represents the relation to our phones. The text can be interpreted as our phone "speeking" to us, telling us how we feel:

- The year of 2012 represents the first text sent on a phone 'Merry Christmas' (sent in year 1992) - a time where messaging where simple.  
- The year of 2013 represents the rise of attention engineers - engineers whose jobs are making us spend as much time as possible on the phone
- The year of 2016 represents a high rise in suicide and selfharm - lots of experts links mobile phones and social media to theese numbers
- The year of 2020 represents the rise of loneliness and use of technology under the pandemic
- The year of 2021 represents the anxiety lots of people get when having a phone 

The program becomes iconic because the purpose of the program is to create awareness of our phone consumption and how it affects us, but it uses element that attention engineers are using when programming social media apps: The red notification color of the circle _(a visual effect that releases dopamine in the brain and can create an addiction)_, the messenger sound _(an audio effect that releases dopamine in the brain and can create an addiction)_, and the text bobble. Another iconic element of the program is the collision between the purpose of the program and the excecution of the program. While the program tries to make the user aware of his/hers/their phone consumption (and hopefully make them reduce it), the user spends a lot of time on exploring the app. Thereby the program plays with the topic 'time' on different levels. 


## Time-related syntaxes
One of the time-related syntaxes can be seen on the picture below. I have used these syntaxes to show the picture and text, when one of the buttons is pressed, and to make sure that the picture and text don't stay in the canvas. 

![Syntax1](syntax1.png)

Previously in the code, I declared the variable 'let on = false' for activating the image and text. For this I have used the if-statement seen above. The statement is activated when one of the buttons is pressed because the variable turns to 'true'. When the framerate is above 1, the counter activates the first if-statement is activated and the image and text is shown on the screen. When the framerate is above 16, the counter activates the second if-statement and the fist if-statement become falls and "turns off".

Another time-related syntax is the thrubber, seen on the picture below. The syntaxes can be seen as time-related syntaxes, because they're defined in the draw function, which replay the syntaxes and thereby makes the loop. When I declare the 'cir' variable, the circle is repeated 8 times (num variable) in 360 degrees. The repetition makes the syntax time-related. 

![Syntax2](syntax2.png)



### Thoughts during the process

**Conceptual thoughts**
During christmas break, I read the book by Carl Newport called Digital Minimalism. The book concerns how our screentime has increased through the years, its impact, and how to decrease the amount of time you spend infront of a screen. The book has had a big influence on my view on electronic products and how they affect our mental health and relationships with other people. I saw the task description for this miniX as a huge opportunity to enligthen this issue by programming. 

**Thoughts during programming**
I had a lot of fun when I created the program, and discovered the percs of using boolean syntaxes.

## Information

### Sources
- Newport, C. (2020). _Digital Minimalism Choosing a Focused Life in a Noisy World_. Penguin Books Ltd
- TED Talk. (2017, august 1). _Why do screens make us less happy_ [Video]. Youtube. https://www.youtube.com/watch?v=0K5OO2ybueM&t=16s 
- Twenge, J. M. (2020). _Increases in Depression, Self‐Harm, and Suicide Among U.S. Adolescents After 2012 and Links to Technology Use: Possible Mechanisms_. Located 16 february 2021 at https://prcp.psychiatryonline.org/doi/10.1176/appi.prcp.20190015 
- WikiPedia. (2020). _Neil Papworth_. Located 16 february 2021 at https://en.wikipedia.org/wiki/Neil_Papworth 


### Task description
1. Experiment with various data capture input and interactive devices, such as audio, mouse, keyboard, webcam/video, etc.
2. Develop a sketch that responds loosely to the transmediale open call “Capture All,” https://transmediale.de/content/call-for-works-2015. (Imagine you want to submit a sketch/artwork/critical or speculative design work to transmediale as part of an exhibition.)


### Questions of the week
**What do you want to explore and/or express?**
**What are the time-related syntaxes/functions that you have used in your program, and why have you used the syntax in this way? How is time being constructed in computation (refer both to the reading materials and your coding)?**
**Think about a throbber that you have encounted in digital culture, e.g. for streaming video on YouTube or loading the latest feeds on facebook, or waiting for a payment transaction, and consider what a throbber communicates, and/or hides? how might we characterize this icon differently?**


