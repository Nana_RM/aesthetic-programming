//DECLARING VARIABLES
let size = 10; //Size of the rotating circle, before pressing a button
let textState = 0; /*The value of the sentences before choosing a button.
OBS: The value changes when pressing a button and isn't activated before
pressing the button*/
let on = false; //A false statement. A true statement is added in function button
let counter = 0/*When the counting begins. See 'statement for activating sound
and image'*/
let textArray = ['Merry Christmas', 'Give me attention!!!', 'Omg, you got so many friends!',
'You are not pretty enough', 'Why do you not just kill yourself', 'This video is so funny','20 unread messages',
'Do not look at them, look at me', 'You feel lonely? I am here for You', 'You are addicted'];


var message; //message picture
var sound;//message sound

//LOADING PICTURE AND SOUND
function preload(){
  message = loadImage('message.png');
  sound = loadSound('facebook.mp3');
  }

function setup(){

  //CREATING CANVAS AND SETS THE FRAMERATE
  createCanvas(1000,700);
  frameRate(8);

//CREATING BUTTONS
  buttonTwo = createButton('Year 2012');//Creating the button
  buttonTwo.position(50,600);//placing the button on the canvas
  buttonTwo.mousePressed(yearTwo);/*when the button is pressed 'function yearX
  activates'*/

  buttonThree = createButton('Year 2013');
  buttonThree.position(150,600);
  buttonThree.mousePressed(yearThree);

  buttonFour = createButton('Year 2014');
  buttonFour.position(250,600);
  buttonFour.mousePressed(yearFour);

  buttonFive = createButton('Year 2015');
  buttonFive.position(350,600);
  buttonFive.mousePressed(yearFive);

  buttonSix = createButton('Year 2016');
  buttonSix.position(450,600);
  buttonSix.mousePressed(yearSix);

  buttonSeven = createButton('Year 2017');
  buttonSeven.position(550,600);
  buttonSeven.mousePressed(yearSeven);

  buttonEight = createButton('Year 2018');
  buttonEight.position(650,600);
  buttonEight.mousePressed(yearEight);

  buttonNine = createButton('Year 2019');
  buttonNine.position(750,600);
  buttonNine.mousePressed(yearNine);

  buttonZero = createButton('Year 2020');
  buttonZero.position(850,600);
  buttonZero.mousePressed(yearZero);

  buttonOne = createButton('Year 2021');
  buttonOne.position(950,600);
  buttonOne.mousePressed(yearOne);
}

//PLAYING THE MESSENGER SOUND
function messagePlaying(){
sound.play();/*Playing the sound. Plays constantly if not put in the 'function
yearX'*/
sound.setVolume(0.6);//volume of file
}


function draw(){
  //DRAWING THE BACKGROUND
  background(255,150);

//STATEMENT FOR ACTIVATING SOUND AND IMAGE
if(on==true){//on is activated in 'function yearX'
  counter++;//setting a counter
  if (counter>1)
  image(message,100,235,230,230);/*message activates when the counter is bigger
  than 1*/
  text(textArray[textState], 125, 355);/*text activates when the counter is bigger
  than 1*/
}
  if(counter>16){ /*When the counter hits frame number 16 (the thrubber have
  been running two times - look at MAKING THE THRUBBER) the message stops*/
    counter=0;
    on=false;
  }

//TEXT
 push();
    textSize(28);
    textStyle(NORMAL);
    stroke(10);
    fill(50);
    textAlign(CENTER);
      text('Are we becomming addicted to our phones?',500,100);

      textSize(16);
      textStyle(NORMAL);
      stroke(1);
      fill(50);
      textAlign(CENTER);
        text('Choose a year and see the phone consumption of the year',500,150);
pop();

  drawElements();
}

function drawElements(){

//MAKING THE THRUBBER
push();
  let num = 8;//declaring a variable - the number of cirkles
  translate(1000/2, 700/2);
  let cir = 360/num * (frameCount%num);//the degree of the thrubber many
  rotate(radians(cir));//The thrubber rotates in circles
  noStroke();
  fill(250,62,62);
  circle(35,0,size);
  pop();



}
//FUNCTIONS FOR BUTTONS
function yearTwo(){ //functions of the button
    size = 15;//the size of the circle
    textState = 0;//The number of the sentence in textArray
    on = true;//Look at STATEMENT FOR ACTIVATING SOUND AND IMAGE
    messagePlaying();//The message sound plays one time

}

function yearThree(){
    size = 20;
    textState = 1;
    on = true;
    textState = 1;
    on = true;
    messagePlaying();
  }

function yearFour(){
  size = 24;
  textState = 2;
  on = true;
  messagePlaying();
}

function yearFive(){
  size = 34;
  textState = 3;
  on = true;
  messagePlaying();
}

function yearSix(){
  size = 37;
  textState = 4;
  on = true;
  messagePlaying();
}

function yearSeven(){
  size = 45;
  textState = 5;
  on = true;
  messagePlaying();
}

function yearEight(){
  size = 52;
  textState = 6;
  on = true;
  messagePlaying();
}

function yearNine(){
  size = 57;
  textState = 7;
  on = true;
  messagePlaying();
}

function yearZero(){
  size = 64;
  textState = 8;
  on = true;
  messagePlaying();
}

function yearOne(){
  size = 70;
  textState = 9;
  on = true;
  messagePlaying();
}
