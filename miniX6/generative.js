//for color (RGB)
var r;
var g;
var b;

//for the movement
var speedX = 20; /* The distance between the dots (in pixlr) on the x-axis. Can
   influence how fast we experience the movement of the dots.*/
var speedY= 15; /* The distance between the dots (in pixlr) on the y-axis. Can
   influence how fast we experience the movement of the dots.*/

//x and y
var x;
var y;

let feMale;//feMale icon

function preload(){
  feMale = loadImage('FeMale.png');
}

function setup(){
//CANVAS
  createCanvas(1200,600); //Size of the canvas
  background(255); // White background

//SPEED
  frameRate(20);/* The frameRate - if the framerate is low, the dots move slow
  in a chopping like motion*/

//START POSITION
  x = width/2; //center of the x-axis
  y = height/2; // center of the y-axis

}

function draw(){

//FEMALE PICTURE
image(feMale,530,250, 150,100);//name, position, size 

//COLOR CHANGE
if (x < 600 && y > 0){
  fill(random(0),random(0),random(150,255)) /* Using RGB. This creates a random
  blue color between the values 150 and 255*/
  } else {
fill(255,random(105,182),random(180,193)) /* Using RGB. This creates a random
   pink color - first rgb is light pink, second is hot pink*/
  }

//MOVEMENT
for(let i = 0; i<20; i++){ /*the loop stops when i < 20. The value of the
     condition has an influence on the speed of the movements of the dots*/
noStroke();
x = x + random(-speedX, speedX); /* The variable is declared in the beginning.
    Chooses a random value between -20 and 20 to adds it to the current position */
y = y + random(-speedY, speedY);/* The variable is declared in the beginning.
    Chooses a random value between -15 and 15 to add to the current position */
circle(x,y,20);//Size of the circle is 20 pixlr
}

}
