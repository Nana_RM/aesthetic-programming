## Boundaries
The human has always been playing with the societal boundaries - from children being naughty, to adults who revolutionize the world. In the past hundred years, our perception of genders and genderroles have changes drastically. Today, it has become more acceptable for men to be more feminin and do the 'motherly' task aswell as women to be more masculine and do the 'manly' tasks. 

**Links: **
Link to program: https://nana_rm.gitlab.io/aesthetic-programming/miniX6/
Link to code:  https://gitlab.com/Nana_RM/aesthetic-programming/-/blob/master/miniX6/generative.js

_Boundaries_ is a program that focus on the boundaries of the gender roles of men and women. The boundarie of the gender roles is set by a coded "pixel line". The pixel line is set at 600 pixel on the x-axis, meaning that circles below 600 pixel on the x-axis shall be the "boy color" blue, while circles above 600 pixel on the x-axis shall be the "girl color" pink. The pixel line is a visualization the societal boundaries of genders - not only the colors that is percieved as girl or boy colors but also what the genders should behave and look like. 
_Boundaries_ does not only work with the visualization of the gender boundaries, but also boundaries that is set for the program. I've set two rules of the program:

1. When one circle is drawn at the canvas, another one will appear at a random location close to the previous dot
2. If the position of the circle is below 600 on the x-axis, the dots turns blue. If not, the dots turns pink.

When _Boundaries_ is started, it is interesting to see how the program actually push the boundaries of the rules that I have set. As seen on the screenshot below, the program doesn't always follow rule number 2. Sometimes the pink and blue colours get mixed (e.g. look at the top of the screenshot, left side). In this way _Boundaries_ how the boundaries of the genderroles always is elvoving and creates a new perception of the word 'man' and 'woman'. Even though we sometimes set clear bounderies, the will always be pushed, like the program is pushing the boundaries I have set by not following the rules.   



![Screenshot](boundaries.png)


### Critical making and the connection to the previous miniX
**My previous miniX**

_Boundaries_ is a further development of my minix5. The program follows the same rules as the rules of _Boundaries_ except for the colours are red and blue. As mentioned in the section _Thoughts during the process_ I wanted to focus on the gender roles in our society, showing that the boundaries always will be pushed.

**Critical making in _Boundaries_**
When reading the word 'programming', lots of people think about mathematics, creating website, games etc. Programming can also be used for creating digital art and bring the discussion of societal issues to life. This is also known as aesthetic programming, where the maker/programmer uses code to reflect on technical, cultural and political aspects. 
   For my miniX6, I have changed my previous miniX to an aesthetic program that reflect on the gender roles in the society and the boundaries we set for these roles. _Boundaries_ is a reflective design, that encourage the user to reflect on the boundaries we set for the different genders' behaviours and appearances. As Ratto and Hertz mentions in the book _The Critical Makers_, reflective design is an important part of critical making, the encouragement of reflection is an important part of critical making:  

"[...] reflective design encourages the development of technical objects intended to encourage reflection on the part of users. In doing so, reflective design practitioners aim to denaturalize the passivity of the typical relations between technology consumers and producers." (Bogers & Chiappini, 2019, p. 22)

As previously mentioned, _Boundaries_ does as well focus on the reflection of the user. The reflection of gender roles will particulair appear when looking at the circles colours and how the program follows the rule of the program. 


### Thoughts during the process
A few days ago, my uncle and I talked about the life of my grandfather. I was told that when they where children, my grandfather worked in another city, which meant that he only saw his wife and children on the weekends. In 1969 he choose to become a stay-at-home dad, while my grandmother would have two part time jobs. When the people living in the street found out, the men wouldn't even talk to him - ignoring him because he choose his children above his job. During the years, their house became the best playground for children, since his company opened up for new and fun games. The story of my grandfather created a debate about the development of equality and view on genders throughout the years. This became an inspiration for my new miniX 

## Information

### Sources
- Bogers, L. & Chiappini, L. (2019). _The Critical Makers Reader: (Un)learning Technology_. Amsterdam: Institute of Network Cultures

Link to image: https://www.pngwing.com/en/free-png-bogzf/download



### Task description
Revisit/Rework one of the earlier mini exercises that you have done, and try to focus on the methodology of Aesthetic Programming and consider technology as a site of reflection. Rework your ReadMe and RunMe

### Questions for this week
- Which MiniX do you plan to rework?
- What have you changed and why?
- How would you demonstrate aesthetic programming in your work?
- What does it mean by programming as a practice, or even as a method for design?
- What is the relation between programming and digital culture?
- Can you draw and link some of the concepts in the text (Aesthetic Programming/Critical Making) and expand on how does your work demonstrate the perspective of critical-aesthetics?
