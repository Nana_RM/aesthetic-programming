## Exploration of Circles

Seen on the picture below is my first project called "Exploration of Circles" created with the p5.js library. The project explores the different elements that can be applied to the circle shape by playing with different colors, sizes and transparency. When pressing on the canvas, the colors of the circles will change to a ligther or darker color. 

 ![](Skærmbillede_2021-02-05_kl._14.20.40.png)
 
**Links:**

Link to the program: https://nana_rm.gitlab.io/aesthetic-programming/miniX1/

Link to the code: https://gitlab.com/Nana_RM/aesthetic-programming/-/blob/master/miniX1/sketch.js

## Thoughts during the process

**The programming language**
When I made the program I found some similarities between coding, literacy and math. When coding you need to have a mathematical approach in order to write and understand the references. The figures are positioned in a coordinate system, and when coding You'll write the value of X and Y to place the figure on the canvas. The placement of the code in the js file have a big impact on the outcome. Colours, strokes etc. have to be placed before the code of the circle to indicate that it belongs to the specific circle. Coding can also be connected to literacy. As Annette Vee writes in her book "Coding for Everyone and the Legacy of Mass Literacy", literacy doesn't only concern the skills of reading and writing: "Literacy… [is] not just  the process of learning the skills of reading, writing, and arithmetic, but a contribution to the liberation of man and to his full development." (Vee, 2017p. 60) (Vee, 2017, p. 45). As written, literacy doesn't only concern the skills of reading and writing but skills that given the individual potential to liberate him-/herself. Due to the societal and technological development, learning to code can also be seen as an important skill, that can liberate the individual. If the individual understands coding and can create his/her own project, he/her can experience a individual impowerment, learning new ways to think, be cricitial to technology and improve the 
employability and economic concerns (Vee, 2017, p. 79-87).


## Information

### Sources
- Vee, A. 2017. "Coding for Everyone and the Legacy of Mass Literacy", Coding Literacty: How Computer Programming Is Changing Writing. Cambridge. MIT Press. P. 43-93.

### Task description
1. Study at least one example of syntax from the p5.js reference site, https://p5js.org/reference/. (Of course, it is always good to know more than one. Be curious!)
2. Familiarize yourself with the reference structure: examples, descriptions, various pieces of syntax and parameters (This knowledge will give you an essential, lifelong skill for learning new syntax on your own).
3. Use, read, modify (or even combine) the sample code that you find (the most basic level is changing the numbers), and produce a new sketch as a RunMe.

### Questions for the ReadMe
- How would you describe your first independent coding experience (in relation to thinking, reading, copying, modifying, writing code, and so on)?
- How is the coding process different from, or similar to, reading and writing text?
- What does code and programming mean to you, and how does the assigned reading help you to further reflect on these terms?
