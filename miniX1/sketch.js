function setup(){
  createCanvas(1400,700);
  background(255,254,183);

}

function draw(){

if (mouseIsPressed) {
  //This is the new colors

  //top purple
  noStroke();
  fill(102,26,125);
  circle(640,-40,400);

  //top brown
  noStroke();
  fill(112,81,28);
  circle(390,70,60);

  //top orange and peach
  noStroke();
  fill(255,197,129);
  circle(1100,100,120);

  noStroke();
  let c1 = color(226,113,38,80);
  fill(c1);
  circle(1000,150,250);
  let value1 = alpha(c1);

  //center mix
  strokeWeight(40);
  stroke(230,128,77);
  fill(220,81,11);
  circle(200,650,800);

  //center red
  noStroke();
  fill(255,110,110);
  circle(550,550,90);

  //center brown
  noStroke();
  fill(198,161,137);
  circle(700,400,205);

  //left pink
  noStroke();
  fill(255,178,233);
  circle(180,220,170);

  noStroke();
  let c2 = color(235,95,193,102);
  fill(c2);
  circle(230,180,110);
  let value2 = alpha(c2);

} else {
  //This is the present colors

  //top purple
  noStroke();
  fill(234,210,242);
  circle(640,-40,400);

  //top brown
  fill(198,158,123);
  circle(390,70,60);

  // top orange and peach
  noStroke();
  fill(255,130,4);
  circle(1100,100,120);

  noStroke();
  let c1 = color(255,221,186,80);
  fill(c1);
  circle(1000,150,250);
  let value1 = alpha(c1);

  //center mix
  strokeWeight(40);
  stroke(255,191,78);
  fill(255,176,37);
  circle(200,650,800);

  //center red
  noStroke();
  fill(250,57,57);
  circle(550,550,90);

  //center brown
  noStroke();
  fill(125,79,38);
  circle(700,400,205);

  //left pink
  noStroke();
  fill(255,57,130);
  circle(180,220,170);

  noStroke();
  let c2 = color(255,191,223,102);
  fill(c2);
  circle(230,180,110);
  let value2 = alpha(c2);
}

  //text in front
textSize(35);
fill(50);
text('PRESS HERE',1050,600);




}
