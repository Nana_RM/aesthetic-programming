//DECLARING VARIABLES//
let goodManSize = { //size of Dad
  w:170,
  h:170
};
let goodMan; // dad
let goodManPosY; //beginning postion of Dad
let mini_height;
let min_speechBubble = 5;  //min speechbubbles on the screen
let speechBubble = [];
let score =0, lose = 0; //beginning scores
let keyColor = 45;
let dearDaddy; //soundfile
let girl; //girl
let badMan; //bad man

//LOADING IMAGES AND SOUNDFILE
function preload(){
  goodMan = loadImage("Data/goodMan.gif"); //dad
  badMan = loadImage("Data/badMan.gif"); //bad man
  girl = loadImage("Data/girl.png"); // girl
  speechBubbleImg = loadImage("Data/speechBubble.png") // speechbubble

  dearDaddy = loadSound('Data/dear-dad.mp3'); // soundfile
}


function setup() {
  createCanvas(windowWidth, windowHeight);
  goodManPosY = height/2; // beginning position of Dad
  mini_height = height/2; //

//PLAYING SOUND//
  dearDaddy.play(); // playing the sounfile
}


function draw() {
  background(240);
  fill(keyColor, 255); //keycolor
  rect(0, height/1.5, width, 1); // the seperation line of characters and scores

//other functions
  displayScore();
  checkSpeechBubbleNum(); //available speechbubbles
  showSpeechBubble();
  hitting(); //scoring/hitting

//showing characters
  image(goodMan, 70, goodManPosY, goodManSize.w, goodManSize.h); // dad
  image(girl, -30, height/4, 140, 140); // girl
  image(badMan, windowWidth-150, height/4, 170,170); // bad guy
}

//CREATING SPEECHBUBBLES//
function showSpeechBubble(){
  for (let i = 0; i <speechBubble.length; i++) {
    speechBubble[i].move();
    speechBubble[i].show();
  }
}


//NEW SPEECHBUBBLES//
function checkSpeechBubbleNum() {
  if (speechBubble.length < min_speechBubble) { //when min speechbubbles is
    //reached, a new one is created
    speechBubble.push(new SpeechBubble()); //creating a new speechbubble
  }
}

//DAD AND SPEECHBUBBLES COLlIDING
function hitting() {
  //calculate the distance between each speechbubble
  for (let i = 0; i < speechBubble.length; i++) {
    let d = int(dist(goodManSize.w/2, goodManPosY+goodManSize.h/2,  speechBubble[i].pos.x, speechBubble[i].pos.y)
      );
    if (d < goodManSize.w/2.5) { //if dad is close enough to the speechbubble
      score++; //adding points to shutdown comments
      speechBubble.splice(i,1); //deleting speechbubble
    }else if (speechBubble[i].pos.x < 3) { //if dad misses a speechbubble
      lose++; //adding points to not done anything
      speechBubble.splice(i,1); //deleting speechbubble
    }
  }
}

//TEXT//
function displayScore() {

//displaying score
    fill(keyColor, 160);
    textSize(17);
    text('You have shut down '+ score + " comment(s)", 10, height/1.4);
    text('You have not done anything ' + lose + " time(s)", 10, height/1.4+20);
    fill(keyColor,255);
    text('PRESS the ARROW UP & DOWN key to shut down the comment(s)',
    10, height/1.4+40);

//Dead Daddy text
  textSize(17);
  text(
  'Dear Daddy. One thing always leads to another. So please stop it before it gets the chance to begin.',
  200, height/1.4+100);

}

//MOVING DAD
function keyPressed() {
  //moving dad
  if (keyCode === UP_ARROW) {
    goodManPosY-=50;
  } else if (keyCode === DOWN_ARROW) {
    goodManPosY+=50;
  }
  //reset if DAD moves out of range
  if (goodManPosY > mini_height) {
    goodManPosY = mini_height;
  } else if (goodManPosY < 0 - goodManSize.w/2) {
    goodManPosY = 0;
  }
}
