class SpeechBubble {
    constructor()
    { //initalize the objects
    this.speed = floor(random(3, 6));
    //floor(n) chooses a random value between 3 and 6. 
    this.pos = new createVector(width+5, random(12, height/1.7));
    this.size = floor(random(50, 200));
    //Chooses a randon size
    }
  move() {
    this.pos.x-=this.speed;  //i.e, this.pos.x = this.pos.x - this.speed;
  }
  show() {

image(speechBubbleImg, this.pos.x ,this.pos.y, this.size, this.size);
}
}
