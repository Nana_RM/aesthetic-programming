## Dear Daddy
On the evening on of 3 march, a Sarah Everard disappeared in Southern Londan on her way home. A few days later, Sarah was found dead in woodland near Ashford. The death of Sarah has lead to a movement, discussing the unfairness of womens fear of going home alone in the dark. But what is the reason for women being scared of walking home alone in the dark?  

One of the reasons that women are scared to walk alone, can be rooted to the socital tollerance of things that seem banal or innoncence. Being called a bitch, getting groped, catcalled, or told the 'Make me a sandwich' joke isn't unusual for women to experience in their day to day life. These comments and actions might seem innocent but have a  longterm effect on the way that women are being treated. The Norwefian charity organization, CARE, adresses this issue in the video "Dear Daddy", that's a wake-up video for violence against girls and women. Throughout the video, they adresses how the sociatal tollerence affects the way that women are being treated. If certain words is constantly used to degrade women, we should stop saying them, because then we are one step closer to fairness between men and women.   


### How does the game work
When the game begins, the player can control the character "Daddy" by moving him up or down. Daddy is protecting a girl (or women) from the insults from another man, seen on the right side on the canvas. In order to shut down the comments, Daddy will have to block the comments from the other man. When blocking a speechbubble, Daddy gets a point at "You shut down x comment(s)". When not blocking a speechbubble, Daddy gets a point at "You have not done anything x time(s)". 

Remember to turn up the volume before playing the game :) 

![screenshot](dearDaddy.png)

link to program: https://nana_rm.gitlab.io/aesthetic-programming/minix7/

link to code: 
- the game: https://gitlab.com/Nana_RM/aesthetic-programming/-/blob/master/minix7/game.js
- the object: https://gitlab.com/Nana_RM/aesthetic-programming/-/blob/master/minix7/speechbubble.js 

### Conceptual thoughts
As previously mentioned, this miniX is a comment to the violence against women. In order to implement the critical thinking of violence against women, I've focused on reflection throughout the process. As Boggers & Chiappini mentions, it's a important part of 'critical' in the critial making:

_"We believe that 'critical' is an essential attribute for such interdisciplinary practice, including reflective thinking about the value propositions and epistemic boundaries and practices within traditional disciplines." _(Boggers & Chiappini, 2019, p. 20)

My reflectiontion throughout the process can be read in this the section belov. This part focus specific on the main problem on the violence, not the visual expression. 

During the programming of the game, I've asked myself questions that can be connected to the visual expression: 
- Why doesn't the game never end? _You can't just eliminate the problem by shutting down a fed comments and leave. We will constantly have to shut down these comments in order to change the way we view girls and women. _
- why do the male characters look alike? _We might all have said an insulting joke or objectified women in some other way - men aswell as women. In order to show that the person saying insulting jokes, sexually assulting, or raping women can be someone You know, I've made the male characters the same. _

## Thoughts during the process

**Discussion with friends and family**

In the past year, I've been discussing some of the worries and experiences that girls and women might have, when it comes to interacting with (bad) men. Through these discussions I've been aware of how bad some men treats women, how often it happens and how it affects us. In the middle of march the Sarah Everard's case caught a lot of womens' attention raising the question, why women should feel afraid when walking home alone. Throughout the week I was talking about the issue with women and girls I know, and was remined how big of a problem it was. Some of the women/girls mentioned: 

- "I always walk in the middle of the road, when I'm walking home. I'm afraid someone is hiding in the bushes."
- "My friend (man) was speechless when I told him how many of the girls I knew who had been physically abussed, hearassed, sexually assulted and raped. I wish I could walk through Botanisk Have like him, withouth being afraid."
- "I've been looking for a peberspray. I'm afraid to walk alone in the dark - especially when strangers apporach me."
- "Years ago, I was raped by a man who was working at the police."  
- "When I'm walking home in the dark I always have my keys in the hands. In case someone wants to attack me, I might have a chance..."
- "I can't go out and drink a beer or party with my friends withouth a guy ruining my night. It happens all the time! Last time I was drinking a beer with my friends in the city center, I had to reject a guy 15 times. The 16th time I stuck up my face in his, shouted that he should go back to his table and stay there, or I would kick him in the nuts. Quickly, his friend came to get him, and just laughed at the situation - my friend is just drunk, he told me. Yeah, it's funny that You let him ruin my night, by laughing at your table each time I rejected him, instead of doing something, I told him."   

Unfortunatly it seemed like a lot of men weren't aware of these issues, and how many women has been a victim of sexually assult, attacks and rape. 

During the week I've been wondering why some men are sexually assulting, attacking and rapeing women. One day the short video "Dear Daddy" appeared on my screen and it made me aware of why some men are acting like that and how f*cked the society is. 

**Reflecting on the issue**

After I rewatched the short film 'Dear Daddy', my view on the "text me when you get home" movement (Sarah Everd's case) changed. I believe the issue of how women are treated is due to the way that we as a society articulates women and girls. Some of the instances can be calling them whores, telling insulting jokes, not believing in their abilities etc. Dear Daddy portraits one of these issues when saying:


"... you see, I will be born a girl which means that by the time I'm 14 the boys in my class will have called me a whore, a bitch, a cunt and many other things. It’s just for fun of course, something boys do. So you won’t worry and I understand that."

This shows that we have accepted the way that girls are treated because it's "something boys do". These ways of articulating girls and women affects the way some men treats women. Objectifying them by grapping them on inappropriate places, raping them etc. This can draw parallels to Oprahs social experience with racism (see link) that shows how our inviroment can the way we view other people. 

Therefore I wanted to make a minix that adresses the root of the problem. Instead of preventing voilence and sexually assult against women, we should prevent it! As told in Dear Daddy:

"One thing always leads to another. So please stop it before it gets the chance to begin. Don’t let my brother’s call girls whores because they’re not. And one day some little boy might think it's true.  Don't accept insulting jokes from weird guys by the pool or even friends. Because behind every joke there's always some truth."


### Technical aspects

**Object abstraction**
For this game i made the speechbubbles as an object. In order to make the speechbubbles an object, I used the four Rules mentioned in the book of Soon and Cox: 

class SpeechBubble {
    constructor()
    { //initalize the objects
    this.speed = floor(random(3, 6));
    //floor(n) chooses a random value between 3 and 6. 
    this.pos = new createVector(width+5, random(12, height/1.7));
    this.size = floor(random(50, 200));
    //Chooses a randon size
    }
  move() {
    this.pos.x-=this.speed;  //i.e, this.pos.x = this.pos.x - this.speed;
  }
  show() {

image(speechBubbleImg, this.pos.x ,this.pos.y, this.size, this.size);
}
}


Step one: naming
_First i named the object Speechbubbles, using a big letter in order to indicade that I'm creating a Class. _

Step two: proporties
_Then I'm creating the properties of the object, by using 'this.', e.g. 'this.speed' _

Step three: Behaviors
_For the third step, I'm creating the behavior of the object - indicading what my object should do. For this object I'm making the object move from right to left. _

Step four: Object creation and usage
_In order to show my speechbubbles on the canvas, I'm declaring the speechbubble as a variable in my program and using the function checkSpeechBubbleNum() and function showSpeechBubble(), which can be seen in the game.js file, to create the speechbubbles and make sure that there'll always be 5 on the canvas. _


## Information

### Sources

Critical making:
Boggers & Chiappini: The Critical Readers CRITICAL MAKING AND INTERDISCIPLINARY LEARNING: MAKING AS A BRIDGE BETWEEN ART, SCIENCE, ENGINEERING, AND SOCIAL INTERVENTIONS (write differently)



Inspiration for the coding:
- Eating tofu sample code: Soon Winnie & Cox, Geoff, "Getting started", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 149-152. 

**Link to short film:**
https://www.youtube.com/watch?v=gOk_qxkBphY

**Dear Daddy Script:** 
http://www.ap8fiercefemales.com/womensselfdefenceblog/2015/12/18/dear-daddy-i-will-be-called-a-whore

Dear Daddy, I just wanted to thank you for looking after me so well, even though I'm not yet born. I know you already try harder than Superman. You won’t even let mommy eat sushi. 

But I need to ask you a favor. Warning. It’s about boys. Because you see, I will be born a girl which means that by the time I'm 14 **the boys in my class will have called me a whore, a bitch, a cunt and many other things. It’s just for fun of course, something boys do.** So you won’t worry and I understand that. **Perhaps you did the same when you were young, trying to impress some of the other boys.** I'm sure you didn't mean anything by it. Still some of the people won’t get the joke, and funnily enough it isn’t any of the girls. It’s some of the boys.

So **by the time I turned 16, a couple of the boys would have snuck their hands down my pants while I'm so drunk I can't even stand straight. And although I say no, they just laugh. It’s funny right.** If you saw me Daddy, you would be so ashamed. Because I'm wasted. 

**No wonder I’m raped when I’m 21.** 21 and on my way home in a taxi driven by the son of a guy you went swimming with every Wednesday. The guy who always told insulting jokes, but they were of course only jokes, so you laughed. Had you known his son would end up raping me you would've told him to get a grip. But how could you know, he was just a boy telling weird jokes. And in any case it wasn't your business, you were just being nice. **But his son, raised on these jokes becomes my business**.

Then finally I meet mister perfect and you're so happy for me Daddy because he really adores me, and he's smart with a great job and all through the winter he goes cross country skiing three times a week just like you. But one day he stops being Mister perfect and I don't know why. Wait, am I overreacting? One thing I do know, I'm not the victim type. I'm raised to be a strong and independent woman. **But one night it's just all too much for him, with work and the in laws and the wedding coming up, so he calls me a whore.** Just like you called a girl in middle school a whore once. **Then another day, he hits me.** I mean, I'm way out of line, I can really be a bitch sometimes but we're still the world's greatest couple and I'm so confused because I love him and I hate him and I’m not sure if I really did do something wrong and, and **then one day he almost kills me.** It all goes black. Even though I have a PHD, a fantastic job, I’m loved by my friends and family, I'm well brought up and nobody saw this coming. 

**Dear Daddy, this is the favor I want to ask. One thing always leads to another. So please stop it before it gets the chance to begin. Don’t let my brother’s call girls whores because they’re not. And one day some little boy might think it's true.  Don't accept insulting jokes from weird guys by the pool or even friends. Because behind every joke there's always some truth. Dear Daddy, I know you will protect me from lions, tigers, guns, cars and even sushi without even thinking about the danger to your own life. But dear Daddy, I will be born a girl. Please do everything you can so that that won’t stay the greatest danger of all.**

Oprah experience with racism: 
https://www.youtube.com/watch?v=ebPoSMULI5U 

### Task Description
Think of a simple game that you want to design and implement. Which objects are required? What are their properties and methods? At the most basic level, you need to use a class-based object-oriented approach to design your game components. These can exhibit certain behaviors which means you need to at least have a class, a constructor, and a method. Once you understand objects and classes, you can continue to work on a mini game implementing interaction with objects. Start with something simple in terms of thinking or reappropriating the rules, interactions and checking steps. The Eating tofu sample code and other games that mentioned above are useful for practicing building the logics and combining smaller steps.

### Questions of the week

- Describe how does/do your game/game objects work?
- Describe how you program the objects and their related attributes, and the methods in your game.
- Draw upon the assigned reading, what are the characteristics of object-oriented programming and the wider implications of abstraction?
- Connect your game project to a wider cultural context, and think of an example to describe how complex details and operations are being “abstracted”?
