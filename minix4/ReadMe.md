### Innocence - _A critic of cookies_
The word 'Cookie' might sound innocent and seem a bit tempting, but are cookies as innocet as they sound? Today, lots of people don't consider what they agree to, when pressing the agree button on a homepage. When trying to get a grips on the terms and conditions of entering a homepage, the amount of information might feel overwhelming causing a lot of people to just press the accept button. This program is a critic to the cookies of homepages, and the indifferent role they might play in peoples lives. I have choosen to describe each side of the program and the thoughts behind it in the sections below. 

**Links**
Link to the program: https://nana_rm.gitlab.io/aesthetic-programming/minix4/

Link to the code: https://nana_rm.gitlab.io/aesthetic-programming/minix4/cookie.js/

## Frontpage
When entering the program, the user will be met by the frontpage of The Times with a 'cookie consent' pop up window. The user has three optionts if he/she/they wants to get access to the homepage: Look at the privacy policy, change the cookie settings or agree to the terms. When clicking on one of the buttons, the user will be met by a program that will critic the button. This will be described in the following sections.   

![Screenshot](https://nana_rm.gitlab.io/aesthetic-programming/minix4/Frontpage.minix4.png/)



## Privacy Policy
When the user has pressed the 'Privacy Policy' button on the frontpage, he/she/they will be let to the program seen below. In this program, the word 'bla' is repeated on a black canvas - going from the left side of the x-axis of the canvas to the right side and then going 10 pixlr down on the y-axis. The word 'bla' is a critic of the privacy policy on a homepage. Lots of people don't look at the privacy policy because there's writtin to much in the discription. Even though companies wants people to read the terms before accepting, some people just scrool down to the accept button withouth ever reading the terms and conditions. Therefore the privacy policy can be compared to the word 'bla' because both can be seen as indifferent to the user.   

![Screenshot](https://nana_rm.gitlab.io/aesthetic-programming/minix4/PrivacyPolicy.minix4.png/)

## Cookie Settings
When the user has pressed the 'Cookie Settings' button on the frontpage, he/she/they will be let to the program seen below. In this program, the user can press on the checkbox if he, she or they doesn't want the data to be stored. When pressing on the checkbox, it will disappear and appear at a random place on the canvas. Thereby, the user can never check the checkbox of not storing his/hers/their data. This is a critic to the cookie settings. When looking at the cookie settings of homepages, the number of boxes you can uncheck is big and it takes a lot of time to uncheck them all since this has to be done manually. This feeling of irritation can be compared to the same feeling the user might experience in this program. And even thought every checkbox has been checked, do they really not store all of our data?

![Screenshot](https://nana_rm.gitlab.io/aesthetic-programming/minix4/CookieSettings.minix4.png/)


## I Agree
When the user has pressed the 'I Agree' button on the frontpage, he/she will be let to the program seen below. In this program, the user will be met by a map of the world with the text 'Selling Your data to:'. At first, there's no circles on the map, but after a few second one red circle will appear on the map. This might make the user feel releaved that the data only is sold to one place. After a while more red circles appear on the map. This might evoke a feeling of anxiety in the user. The time between the appearing of the circles will decrease after a while and this might increase the users feeling of anxiety. The final amount of red circles can be seen in the image below. This program is a critic to the amount of data that companies shares with other people. When pressing on the agree button, lots of people don't know which people has acces to their data and which places it might end - some of the users might not even give it a thought. To see the sharing of the users data visually on a map might evoke some thoughts in the head of the user.  

![Screenshot](https://nana_rm.gitlab.io/aesthetic-programming/minix4/IAgree.minix4.png/)


## Informations

### Description of the task:
1. Experiment with various data capture input and interactive devices, such as audio,
mouse, keyboard, webcam/video, etc.
2. Develop a sketch that responds loosely to the transmediale open call “Capture All,”
https://transmediale.de/content/call-for-works-2015. (Imagine you want to
submit a sketch/artwork/critical or speculative design work to transmediale as
part of an exhibition.)

### Questions for the task

Provide a title for and a short description of your work (1000 characters or less)
as if you were going to submit it to the festival.
- Describe your program and what you have used and learnt.
- Articulate how your program and thinking address the theme of “capture all.”
- What are the cultural implications of data capture?

### References 

**Inspiration for the conceptual thinking**


**Inspiration sources for the code:**
- The Coding Train. (2017, 26 september). _Coding Challenge #76: 10PRINT in p5.js_ [Video]. Youtube. https://www.youtube.com/watch?v=bEyTZ5ZZxZs 
- The times homepage: https://www.thetimes.co.uk/ 






