//background pictures
let imgOne;
let imgTwo;

//Values for the Cookie Settings button
let x = 0; //start position of the first word
let y = 0; //start position of the first word
let spacing = 20; //space between the words


let on = false; //for Agree button
let no = false;//for Privacy button
/* if the statements are called the same name, both will be released when the
user is led to another page
*/

//For Cookie button
let checkbox = false;


/*IMAGES
Loads pictures of The Times and a map*/
function preload(){
  imgOne = loadImage('screen.png');
  imgTwo = loadImage('map.jpg');
}

function setup(){
//BACKGROUND
  createCanvas(1440,681);//Canvas size
  image(imgOne,0,0);//The Times image set as the background
  frameRate(40);//framerate will be used for buttonAgree

//BUTTON COLOR
/* making a statement for the colors of the buttons. when put the statements as
a global statement, I couldn't change the color of the button */
  let colA = color(69,97,164);
  let colB = color(255);

//CREATING BUTTONS
/* 3 buttons in total. Each button has the same size, a specifik position and a different background
color. There's 151 pixlr between each button on the x-axis */

  //Agree button
    buttonAgree = createButton('I Agree');//name
      buttonAgree.position(825,561);//position
      buttonAgree.size(132,43);//size
      buttonAgree.style('background-color', colA);//background color
        buttonAgree.mousePressed(resetSketch);


  //Cookie setting button
  buttonCookie= createButton('Cookie Settings');
  buttonCookie.position(674,561);
  buttonCookie.size(132,43);
  buttonCookie.style('background-color', colB);
 buttonCookie.mousePressed(cookieSetting);

//Privacy button
  buttonPrivacy = createButton('Privacy Policy');
  buttonPrivacy.position(523,561);
  buttonPrivacy.size(132,43);
  buttonPrivacy.style('background-color', colB);
  buttonPrivacy.mousePressed(writeText);

}


function draw(){

//////////////////////////////buttonAgree///////////////////////////////////////

if (on == true){ /* when the buttonAgree is pressed it activates the resetSketch
  function. This makes on = true, which activates the if-statement*/

//TEXT
  textSize(24);
  fill(50);
    text('Sells Your data to:', 600,640);


/* LOCATIONS
This is the ellipses on the maps. When the frameCount gets bigger than the
stated number, the ellipse will appear.
obs: the framerate is set in the setup function */

//usa
if (frameCount > 100){ //frameCount
  fill(238,0,0); //color
      ellipse(200, 200, 10, 10); //ellipse
}

///now it's 20 frameCounts between each ellipse///

//Brazil
if (frameCount>240){
fill(238,0,0);
    ellipse(400, 450, 10, 10);
  }

  //Australia
if (frameCount>260){
      fill(238,0,0);
          ellipse(1170, 582, 10, 10);
}

//Hong kong
if (frameCount>280){
fill(238,0,0);
    ellipse(1070, 310, 10, 10);
  }

//China
if (frameCount>320){
  fill(238,0,0);
      ellipse(1068, 238, 10, 10);
}

//Cuba
if (frameCount>300){
fill(238,0,0);
    ellipse(289, 316, 10, 10);
}

//Mexico
if (frameCount>340){
fill(238,0,0);
    ellipse(200, 322, 10, 10);
}

//Central Africa
if (frameCount>360){
fill(238,0,0);
    ellipse(650, 400, 10, 10);
}

//Spain
if (frameCount>400){
  fill(238,0,0);
    ellipse(600, 220, 10, 10);
}

//Russia east
if (frameCount>380){
  fill(238,0,0);
      ellipse(1000, 170, 10, 10);
    }

//Russia west
if (frameCount>440){
  fill(238,0,0);
      ellipse(730, 160, 10, 10);
}

//South africa
if (frameCount>420){
fill(238,0,0);
    ellipse(695, 560, 10, 10);
}

///now it's 10 frameCounts between each ellipse///

//India
if (frameCount>450){
fill(238,0,0);
    ellipse(930, 365, 10, 10);
}

//Argentina
if (frameCount>460){
fill(238,0,0);
    ellipse(360, 600, 10, 10);
}

//Middle east
if (frameCount>470){
fill(238,0,0);
    ellipse(770, 300, 10, 10);
}

//Bolivia
if (frameCount>480){
fill(238,0,0);
    ellipse(330, 500, 10, 10);
}

//Denmark
if (frameCount>490){
fill(238,0,0);
    ellipse(646, 160, 10, 10);
}

//Kazahkstan
if (frameCount>500){
fill(238,0,0);
    ellipse(840, 200, 10, 10);
}

//USA 2
if (frameCount>510){
fill(238,0,0);
    ellipse(260, 260, 10, 10);
}

//New Zealand
if (frameCount>520){
fill(238,0,0);
    ellipse(1285, 590, 10, 10);
}

//USA 3
if (frameCount>530){
fill(238,0,0);
    ellipse(140, 240, 10, 10);
}

//India north west
if (frameCount>540){
fill(238,0,0);
    ellipse(900, 300, 10, 10);
}

//India central east
if (frameCount>550){
fill(238,0,0);
    ellipse(950, 330, 10, 10);
}

//England
if (frameCount>560){
fill(238,0,0);
    ellipse(610, 170, 10, 10);
}

//Australia
if (frameCount>570){
    fill(238,0,0);
        ellipse(1225, 530, 10, 10);
}


//USA 5
if (frameCount>580){
fill(238,0,0);
    ellipse(200, 260, 10, 10);
}

//England
if (frameCount>590){
fill(238,0,0);
    ellipse(605, 155, 10, 10);
}

//USA 4
if (frameCount>600){
fill(238,0,0);
    ellipse(290, 230, 10, 10);
}

//Tibet
if (frameCount>610){
  fill(238,0,0);
      ellipse(930, 238, 10, 10);
}

//South africa
if (frameCount>620){
fill(238,0,0);
    ellipse(730, 520, 10, 10);
}

//Peru
if (frameCount>640){
fill(238,0,0);
    ellipse(290, 440, 10, 10);
}

//Central Africa
if (frameCount>660){
fill(238,0,0);
    ellipse(600, 300, 10, 10);
}

//Thailand
if (frameCount>670){
  fill(238,0,0);
      ellipse(1000, 300, 10, 10);
}

//West Africa
if (frameCount>680){
fill(238,0,0);
    ellipse(750, 350, 10, 10);
}

//Central America
if (frameCount>690){
fill(238,0,0);
    ellipse(240, 350, 10, 10);
}

//Bulgaria
if (frameCount>700){
  fill(238,0,0);
    ellipse(700, 220, 10, 10);
}

}

///////////////////////////////buttonCookie/////////////////////////////////////
if (no == true){ /* when the buttonCookie is pressed it activates the
  writeText function. This makes no = true, which activates the if-statement*/

//the speed of the words depends the frameRate

fill(255);//white color
text('bla', x + spacing, y + spacing);/*the repeated word, the distance between
the words of the x- and y-axis*/
x = x + spacing;// makes the 'bla' repeat

if(x > width){ /*When the word reaches the end of the x-axis of the canvas, it
  makes a new line*/
  x = 0 //the placement of the new line on the x-axis
  y = y + spacing //the placement on the new line on the y-axis
}

}

}


function resetSketch(){
//creates a canvas with the map image
  createCanvas(1440,681);
  image(imgTwo,50,50);

//Activates the draw function (buttonAgree)
on = true;

//Hides the buttons
  buttonAgree.hide();
  buttonCookie.hide();
  buttonPrivacy.hide();

}

function writeText(){

  //background of the buttonPrivacy
    createCanvas(1440,700);
    background(0);

//activates the draw function (buttonPrivacy)
no = true

  //Hides the buttons
      buttonAgree.hide();
      buttonCookie.hide();
      buttonPrivacy.hide();
}

function cookieSetting(){
  var x = random(1440); //a random number between 0 and 1440 on the x-axis
  var y = random(700); //a random number between 0 and 700 on the y-axis

//creates a grey canvas
  createCanvas(1440,700);
  background(150);

//Hides the buttons
buttonAgree.hide();
buttonCookie.hide();
buttonPrivacy.hide();

//CHECKBOX
/* Creates a checkbox that appears a random (var x and y) place on the canvas */
checkbox = createCheckbox('Should we not store Your data?', false);
    //flase means unchecked
checkbox.changed(myCheckedEvent);//look af the function
checkbox.position(x, y);

}
function myCheckedEvent(){
  var x = random(1440); //a random number between 0 and 1440 on the x-axis
  var y = random(700);//a random number between 0 and 1440 on the y-axis

  if (this.checked(true)) { /*when the cheackbox is checked, a new one appears
    a random place*/
checkbox.position(x,y);
checkbox.checked(false); /*when the checkbox is checked, it will be unchecked
again*/
  }

}
