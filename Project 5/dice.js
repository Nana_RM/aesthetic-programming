let x = 0;
let y = 0;
let spacing = 20;

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(0);
}

function draw() {
  stroke(20,200,100);
  if (random(7) < 0.5) {
    text('Yeah',x+spacing, y+spacing);
  } else {
    stroke(238,0,0);
textSize(24);
    text('Ha', x+spacing, y+spacing);
  }
  x+=50;
  if (x > width) {
    x = 0;
    y += spacing;
  }
}
