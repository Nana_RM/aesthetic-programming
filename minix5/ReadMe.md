## The endless Road - _A generative art piece_
In the last century, our definition of art has changed drastically. Today, art isn't just an artist painting on a canvas, but can aswell be an artist coding a program. This minix will explore the topic 'generative art' by creating my own piece of generative art. 

**Links:**

Link to program: https://nana_rm.gitlab.io/aesthetic-programming/minix5/

Link to code:  https://gitlab.com/Nana_RM/aesthetic-programming/-/blob/master/minix5/generative.js


### Rules of the program
For this program, I made following rules:

1. When one circle is drawn at the canvas, another one will appear at a random location close to the previous dot
2. If the position of the circle is below 600 on the x-axis, the dots turns blue. If not, the dots turns red.

### Performance through time
When the program is started it will follow the rules mentioned above. First a dot will always be placed in the center of the canvas. The next dot will be placed at a random place between -20 pixlr or +20 pixlr close to the current position on the x-axis and -15 pixlr or + 15 pixlr close to the position on the y-axis. If the dot is placed below 600 pixlr on the x-axis, the dot will turn blue. If it's placed above 600 pixlr on the x-axis, the dot will turn red. 

Seen below is two screenshots of the program. One has been taken a few seconds after the program has started, the other has been taken a while after the program has been running:

_In the beggining of the program:_
![](dot1.png)

_After the program has run for a while:_
![](dot2.png)

These screenshots shows how the program can evolve over time. The rules of the program will always have an influence on the outcome of the program. Dots on the left side will always remain blue and dots on the right side will always remain red. Due to the random placement of the dots and the frameRate that is set in the program, the dots will be placed in different locations - sometimes close to each other, other times far away from each other. This behavior can have an influence on the way that the user experience the program. Since the new dot will be placed close to the previous dot, the viewer might experience the random placement of the dots as the dots are following a road.  

 
### The endless Road's connection with the idea of auto-generator
The endless Road has a big connection with the idea of auto-generator and can question the amount of control that the artist has on the art piece. As mentioned in the secitons above, I have set a couple of rules for the program to follow, but due to the randomness of the program, the outcome will be different each time the program is run. This can set a question of my role in the art piece and who the artist of The endless Role really is - me or the computer? As mentioned in the book of Soon and Cox, generative art can be seen as "[...] a sense of machine creativity that [...] negates intentionality and questions the centrality of human[...]" (Soon & Cox, 2020, p. 125). On one side, it is I who have created a set of rules for the computer to follow and thereby it is I who have created the work. On the other side, It is the computer that controls the placement of the dots and thereby creates the outcome of the program. This can show an amount of computer creativity. This can change the way we look at the definiton of the words 'art' and 'artist' 

## Information

### Sources

**Litterature**
- Soon, W. & Cox, G. (2020.) AESTHETIC PROGRAMMING A Handbook of Software Studies. Open Humanitites Press.

**Inspiration for the code**
- https://editor.p5js.org/awade5/sketches/r_eTjplBi


### Task description
1. Start with a blank sheet of paper. Think of at least **two simple rules** that you want to implement in a generative program.
2. Based on the rules that you set in Step 1, design a generative program that utilizes at least **one for-loop/while-loop** and **one conditional statement**, but without any direct interactivity. Just let the program run. You can also consider using noise() and random() syntax if that helps.


### Questions for this week
- What are the rules in your generative program? Describe how your program performs over time? How do the rules produce emergent behavior?
- What role do rules and processes have in your work?
- Draw upon the assigned reading, how does this MiniX help you to understand the idea of “auto-generator” (e.g. levels of control, autonomy, love and care via rules)? Do you have any further thoughts on the theme of this chapter?
