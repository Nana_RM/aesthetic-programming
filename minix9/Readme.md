## Who would You vote for?

![screenshot](WhoWould.png)


Link to program: https://nana_rm.gitlab.io/aesthetic-programming/minix9/


### What is the program about? Which API have you used and why?

For this week, we have chosen to focus on the way that online newspapers can manipulate our choice when choosing a candidate for the elections. This miniX uses the API of New York Times to access headlines of articles containing the keyword “Trump” or “Biden”. The headlines can be seen as a summary of the overall discourses regarding the two politicians. When looking at the headlines, the viewer can make up his own mind about who to vote for based on the accessed data. 


### Can you describe and reflect on your process in this miniX in terms of acquiring,
processing, using, and representing data? 

Before we started programming the minix of this week, we looked for different types of APIs’ that we could use for the program. Unfortunately, it was difficult to get access to a lot of APIs, and we chose to use the API of The New York Times. We had a hard time getting access to this API and got an API key, from our instructor, from the same website. 

Originally, we wanted to compare contrasting discourses on a given subject in terms of news sites expressing different political views: For instance Fox News VS New York Times. 

### How much do you understand this data or what do you want to know more about? 

We were only able to enter parts of the database linked with our choice of searchword. Thus, we don’t necessarily understand how the data is accessed on a deeper level or how it’s selected based on the tag: Is it presented differently between recipients? 

### How do platform providers sort the data and give you the requested data? 

The data is organized in .json-files, accessed through API (Application programming interface). A ‘client’ is granted access to the .json, the ‘server’, by signing up for an API-key: 
“you need to have an API key, a unique identification number, for authorization in which a client program can make API calls/requests (Soon, s.194)”

### What are the power relations in the chosen APIs? What is the significance of APIs in digital culture?

In this minix, we chose to use two APIs that could show different articles from The New York Times. We used two different keywords “Biden” and “Trump” and the API would show 20 different articles - 10 with the keyword Biden and 10 with the keyword Trump. 
   Every online newspaper will always have a different view on the world - some might like Trump and dislike Biden, while others will like Biden and dislike Trump. By asking the user who they would vote for, and showing the different articles with the two keywords, the user can be affected by the headlines. That’s a power relation, the user only having access to one discourse on a given subject. 

Weizman and Keenan’s description of the term ‘Forensis’ is useful to understand how this piece of software functions as a presentation of a selection of information for the user to read. In other words, the miniX presents the reader “material” for his/her own judgement: 

"Derived from the Latin ‘forensis,’ the word’s root refers to the ‘forum,’ and thus to the practices and skill of making an argument before a professional, political or legal gathering. In classical rhetoric, one such skill involved having objects address the forum. Because they do not speak for themselves, there is a need for a translation, mediation, or interpretation between the — ‘language of things’ — and that of people" (s.205).

 One might argue that our miniX doesn’t deals of with the above mentioned notion of “translation”, since the software randomly only selects data based on the two names. Thus, the discourses read from the headlines are not provided by us, but only reflects the discourses of New York times (Soon et. al, p. 205). 

To link the notion of forensis to an observable phenomenon on Facebook, the common understanding of ‘headline readers’ applies here. Generally, from our own experience, it’s understood as users only reading the headlines of articles, thus making up their minds based on superficial judgements. Still, the users participate in discussion on Facebook. 

‘Forensics’, within modern news sharing on Facebook, thus relies on both the source of the article, but also on the users’ wish to investigate further than reading the headline: The modern ‘forum’ has to take the user’s attention span into consideration. This links to ‘clickbaite articles’. ‘Clickbaite’ is seen as pushing reality to the brink of fiction to achieve clicks, and thus the authenticity of the ‘forensis’ is warped by different factors (links to ‘truth regime’, Rouvroy, Soon et. al, p206).  In contrast to the original notion of the ‘forum’, where the speaker had the full attention of his/her audience during the presentation: 

“Using forensics it is possible not only to detect features or patterns in data, but also to
generate new forms, new shapes, or arguments: to allow data to speak for itself — as witness
in a court of law, for instance — and to uncover aspects of what is not directly apparent in the
material." (Soon, et al. p.205) 

### Try to formulate a question in relation to web APIs or querying/parsing processes
that you would like to investigate further if you had more time.
By presenting data, we have had experience with how data-presentation is always a product of humans selecting a fragment of reality to display. This makes us think about Rouvroy’s  '“algorithmic governmentality”' (s.206). Our way of viewing the world is the result of an algorithmic categorization of humans” and the term ‘truth regime’: How internet based databases functions as the truth when acquiring data (Soon, et al. p. 206). 

###References
- Soon Winnie & Cox, Geoff, "Vocable Code", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 165-186

